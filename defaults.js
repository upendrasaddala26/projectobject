function defaults(object,defaultprops){
    let new_object = object;
    for (let item in defaultprops){
        new_object[item]=defaultprops[item]
    }
    return new_object;
}
module.exports=defaults;

