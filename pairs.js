function pairs(object){
    let pairs_array = [];
    for (let key in object){
        pairs_array.push([key,object[key]])
    }
    return pairs_array;
}
module.exports=pairs;