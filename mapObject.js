function mapObject(object,callback){
    for (let key in object){
        object[key]=callback(key,object[key])
    }
    return object;
}
module.exports=mapObject;