

function invert(object){
    let inverted_object = {};
    for (let key in object){
        value = object[key];
        inverted_object[value] = key;
    }
    return inverted_object;
}

module.exports=invert;

// module.exports = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// let invert = {};
// function invert(object){
//     for (let key in object){
//         result[object[key]]=key;
//     }
//     return result;
// }
// module.exports=invert;
